//
// Created by symeon on 24/01/19.
//

#ifndef RUSHHOUR_MICHEL_JUSSEY_CARLE_NOEUD_SMA_STAR_PLUS_H
#define RUSHHOUR_MICHEL_JUSSEY_CARLE_NOEUD_SMA_STAR_PLUS_H


#include <unordered_map>
#include <unordered_set>
#include <cmath>
#include <memory>
#include "Plateau.h"
#include "valeur_ou_infini.h"

class Noeud_SMA_star_plus {
public:

    explicit Noeud_SMA_star_plus(const Plateau &p, const int &g = 0, std::optional<Plateau> parent = {});

    std::optional<Plateau> parent;

    Plateau plateau;
    valeur_ou_infini<int> cout_f;
    int cout_g;
    double cout_c() const;

    bool aEteEtendu = false;
    std::unordered_set<Plateau> successeurs;

    std::unordered_map<Plateau,valeur_ou_infini<int>> couts_f_oublies;
    valeur_ou_infini<int> min_des_couts_f_oublies();

    void etendre();
};

struct ordreDesCoutsF {
    bool operator()(const Noeud_SMA_star_plus& rhs, const Noeud_SMA_star_plus& lhs) {
        if (rhs.cout_f != lhs.cout_f) {
            return rhs.cout_f < lhs.cout_f;
        }
    }
};

namespace std {

    template <>
    class hash<Noeud_SMA_star_plus>{
    public :
        size_t operator()(const Noeud_SMA_star_plus &n ) const{
            size_t res = 1;
            for (auto v : n.plateau) {
                res *= 51;
                res += std::hash<char>()(v.getName());
                res *= 51;
                res += std::hash<int>()(v.getX());
                res *= 51;
                res += std::hash<int>()(v.getY());
                res *= 51;
                res += std::hash<int>()(v.getLongeur());
                res *= 51;
                res += std::hash<int>()(v.getDirection());
            }
            res *= 51;
            res += std::hash<char>()(n.plateau.voiture_joueur.getName());
            res *= 51;
            res += std::hash<int>()(n.plateau.voiture_joueur.getX());
            res *= 51;
            res += std::hash<int>()(n.plateau.voiture_joueur.getY());
            res *= 51;
            res += std::hash<int>()(n.plateau.voiture_joueur.getLongeur());
            res *= 51;
            res += std::hash<int>()(n.plateau.voiture_joueur.getDirection());
            return res;
        }
    };
}
#endif //RUSHHOUR_MICHEL_JUSSEY_CARLE_NOEUD_SMA_STAR_PLUS_H

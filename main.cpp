#include <iostream>
#include <memory>
#include <vector>
#include <unistd.h>
#include "Plateau.h"
#include "A_star.h"
#include "SMA_star_plus.h"

int main(int argc, char* argv[]) {

    int c;
    std::ifstream input;
    bool optionalInput = false;
    size_t maximum = 100;
    bool solve_with_A_star = false;

    while ((c = getopt (argc, argv, "i:m:a")) != -1) {
        switch (c) {
            case 'i':
                input = std::ifstream(optarg);
                optionalInput = true;
                break;
            case 'm':
                maximum = static_cast<size_t>(std::atoi(optarg));
                break;
            case 'a':
                solve_with_A_star = true;
                break;
            case '?':
                if (isprint (optopt))
                    fprintf (stderr, "Unknown option `-%c'.\n", optopt);
                else
                    fprintf (stderr,
                             "Unknown option character `\\x%x'.\n",
                             optopt);
                return 1;
            default:
                abort ();
        }
    }

    Plateau p;

    if (optionalInput) {
        input >> p;
    } else {
        std::cin >> p;
    }

    std::cout << "Plateau : " << std::endl;
    std::cout << p << std::endl;

    std::cout << "Méthode de recherche : ";
    if (solve_with_A_star) {
        std::cout << "A*" << std::endl;
    } else {
        std::cout << "SMA*+" << std::endl;
        std::cout << "Recherche de solution avec une limite mémoire de " << maximum << std::endl;
    }

    std::optional<std::list<Plateau>> solution;
    if (not solve_with_A_star) {
        solution = SMA_star_plus::chercherSolution(p,maximum);
    } else {
        solution = A_star::chercherSolution(p);
    }
    std::cout << std::endl;

    if (solution) {
        std::cout << "Solution : " << std::endl;
        for (const auto& _ : *solution) {
            std::cout << _ << std::endl;
        }
        std::cout << "Solution en " << solution->size() << " mouvements" << std::endl;
    } else {
        std::cout << "Pas de solution trouvée !" << std::endl;
    }

    return 0;
}
//
// Created by symeon on 25/01/19.
//

#ifndef RUSHHOUR_MICHEL_JUSSEY_CARLE_OPEN_SET_A_STAR_H
#define RUSHHOUR_MICHEL_JUSSEY_CARLE_OPEN_SET_A_STAR_H


#include <unordered_set>
#include "Plateau.h"
#include "Noeud_SMA_star_plus.h"

class Open_Set_SMA_star_plus {
public:
    void insert(const Noeud_SMA_star_plus& noeud);
    Noeud_SMA_star_plus minfCostNode();
    std::optional<Noeud_SMA_star_plus> maxcCostLeaf();
    std::optional<Noeud_SMA_star_plus> find(Plateau p);
    void remove(const Noeud_SMA_star_plus& noeud);
    bool has(const Noeud_SMA_star_plus& noeud);
    bool empty();

    Noeud_SMA_star_plus maxcCostNode();

private:
    std::unordered_set<Plateau> plateaux;
    std::multiset<Noeud_SMA_star_plus,ordreDesCoutsF> ordonneParLesCouts;
};


#endif //RUSHHOUR_MICHEL_JUSSEY_CARLE_OPEN_SET_A_STAR_H

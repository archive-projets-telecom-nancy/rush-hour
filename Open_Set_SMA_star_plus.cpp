//
// Created by symeon on 25/01/19.
//

#include "Open_Set_SMA_star_plus.h"

/*
 * Insère le plateau dans le set des plateaux si il n'y est pas
 * Retire tous les noeud au cout f supérieur à celui de noeud dans le multiset et l'insère si besoin
 */
void Open_Set_SMA_star_plus::insert(const Noeud_SMA_star_plus &noeud) {

    plateaux.insert(noeud.plateau);

    bool shouldInsert = true;
    for (auto it = ordonneParLesCouts.begin(); it != ordonneParLesCouts.end(); ) {
        if (it->plateau == noeud.plateau) {
            if (it->cout_f > noeud.cout_f) {
                it = ordonneParLesCouts.erase(it);
            } else {
                // on a trouvé un noeud au moins aussi bien que noeud, pas besoin de l'inserer
                shouldInsert = false;
                ++it;
            }
        } else {
            ++it;
        }
    }

    if (shouldInsert) {
        auto it = ordonneParLesCouts.insert(noeud);
    }

}

Noeud_SMA_star_plus Open_Set_SMA_star_plus::minfCostNode() {
    return *ordonneParLesCouts.begin();
}

void Open_Set_SMA_star_plus::remove(const Noeud_SMA_star_plus &noeud) {
    plateaux.erase(noeud.plateau);
    for (auto it = ordonneParLesCouts.begin(); it != ordonneParLesCouts.end(); ) {
        if (it->plateau == noeud.plateau) {
            it = ordonneParLesCouts.erase(it);
        } else {
            ++it;
        }
    }
}

bool Open_Set_SMA_star_plus::has(const Noeud_SMA_star_plus &noeud) {
    return plateaux.count(noeud.plateau) > 0;
}

bool Open_Set_SMA_star_plus::empty() {
    return plateaux.empty();
}

std::optional<Noeud_SMA_star_plus> Open_Set_SMA_star_plus::maxcCostLeaf() {
    bool found_leaf = false;
    auto max = ordonneParLesCouts.begin();
    auto second_max = max;
    for (auto it = ordonneParLesCouts.begin(); it != ordonneParLesCouts.end(); ++it) {
        if (not found_leaf) {
            if (not it->aEteEtendu) {
                found_leaf = true;
                max = it;
                second_max = max;
            }
        } else {
            if (not it->aEteEtendu) {
                if (max->cout_c() < it->cout_c()) {
                    second_max = max;
                    max = it;
                }
            }
        }
    }

    if (not found_leaf) {
        return {};
    }

    if (max->plateau == minfCostNode().plateau) {
        return *second_max;
    } else {
        return *max;
    }
}

std::optional<Noeud_SMA_star_plus> Open_Set_SMA_star_plus::find(Plateau p) {
    for (const auto& noeud : ordonneParLesCouts) {
        if (noeud.plateau == p) {
            return noeud;
        }
    }
    return {};
}

Noeud_SMA_star_plus Open_Set_SMA_star_plus::maxcCostNode() {
    auto max = *ordonneParLesCouts.begin();
    for (auto n : ordonneParLesCouts) {
        if (n.cout_c() > max.cout_c()) {
            max = n;
        }
    }
    return max;
}

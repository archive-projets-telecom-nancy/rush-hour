//
// Created by symeon on 25/01/19.
//

#ifndef RUSHHOUR_MICHEL_JUSSEY_CARLE_SMA_STAR_PLUS_H
#define RUSHHOUR_MICHEL_JUSSEY_CARLE_SMA_STAR_PLUS_H

#include <optional>
#include <list>
#include "Plateau.h"
#include "Noeud_SMA_star_plus.h"

struct SMA_star_plus {
    static std::list<Plateau> remonterParents(Plateau fin, Plateau debut, std::unordered_map<Plateau,Plateau> precedent);
    static std::optional<std::list<Plateau>> chercherSolution(Plateau p, size_t limite_memoire);
    static std::set<Noeud_SMA_star_plus, ordreDesCoutsF>::iterator pire_feuille(std::set<Noeud_SMA_star_plus, ordreDesCoutsF> set);
};


#endif //RUSHHOUR_MICHEL_JUSSEY_CARLE_SMA_STAR_PLUS_H

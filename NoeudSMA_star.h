//
// Created by symeon on 22/01/19.
//

#ifndef RUSHHOUR_MICHEL_JUSSEY_CARLE_NOEUD_H
#define RUSHHOUR_MICHEL_JUSSEY_CARLE_NOEUD_H


#include <memory>
#include "Plateau.h"

class NoeudSMA_star {
public:

    NoeudSMA_star(std::optional<std::shared_ptr<NoeudSMA_star>> parent,Plateau p, int g);

    std::optional<std::shared_ptr<NoeudSMA_star>> parent;

    Plateau p;

    int f; // Coût ( g + heuristique )
    int g; // Coût du chemin jusqu'au noeud

    bool successeursGeneres = false;
    void genererSuccesseurs();
    std::optional<NoeudSMA_star> prochainSuccesseur();
    bool aGenereTousSesSuccesseurs();

    bool operator<(const NoeudSMA_star& rhs) const;

    void backup();

    std::optional<Plateau> dernier_successeur_genere = {};
    std::set<Plateau> successeurs;
};


#endif //RUSHHOUR_MICHEL_JUSSEY_CARLE_NOEUD_H

//
// Created by symeon on 24/01/19.
//

#ifndef RUSHHOUR_MICHEL_JUSSEY_CARLE_INT_OR_INFINITY_H
#define RUSHHOUR_MICHEL_JUSSEY_CARLE_INT_OR_INFINITY_H


#include <variant>
#include <cmath>

template <typename T>
class valeur_ou_infini {
public:

    explicit valeur_ou_infini<T>(const std::variant<T, std::monostate> &val) : val(val) {};

    valeur_ou_infini<T>(T i) : val(i) {};

    valeur_ou_infini<T>(std::monostate monostate) : val(monostate) {};

    bool operator==(const valeur_ou_infini<T> &rhs) const {
        return val == rhs.val;
    };

    bool operator!=(const valeur_ou_infini<T> &rhs) const {
        return !(rhs == *this);
    };

    bool operator==(const T &rhs) const {
        if (*this == valeur_ou_infini<T>(std::monostate())) {
            return false;
        } else {
            return std::get<T>(this->val) == rhs;
        }
    };

    bool operator!=(const T &rhs) const {
        return !(*this == rhs);
    };

    bool operator<(const valeur_ou_infini<T> &rhs) const {
        if (*this != valeur_ou_infini<T>(std::monostate()) and rhs == valeur_ou_infini<T>(std::monostate())) {
            return true;
        }
        if (*this == valeur_ou_infini<T>(std::monostate()) and rhs != valeur_ou_infini<T>(std::monostate())) {
            return false;
        }
        return this->val < rhs.val;
    };

    bool operator>(const valeur_ou_infini<T> &rhs) const {
        return rhs < *this;
    };

    bool operator<=(const valeur_ou_infini<T> &rhs) const {
        return !(rhs < *this);
    };

    bool operator>=(const valeur_ou_infini<T> &rhs) const {
        return !(*this < rhs);
    };

    bool operator<(const T &rhs) const {
        return *this < valeur_ou_infini<T>(rhs);
    };

    bool operator>(const T &rhs) const {
        return valeur_ou_infini(rhs) < *this;
    };

    bool operator<=(const T &rhs) const {
        return !(*this > rhs);
    };

    bool operator>=(const T &rhs) const {
        return !(*this < rhs);
    };

    template <typename U>
    friend valeur_ou_infini<U> operator+(const valeur_ou_infini<U> &lhs, const U &rhs) {
        return lhs + valeur_ou_infini(rhs);
    };

    template <typename U>
    friend valeur_ou_infini<U> operator+(const U &lhs, const valeur_ou_infini<U> &rhs) {
        return valeur_ou_infini(lhs) + rhs;
    };

    template <typename U>
    friend valeur_ou_infini<U> operator+(const valeur_ou_infini<U> &lhs, const valeur_ou_infini<U> &rhs) {
        if (lhs == valeur_ou_infini<T>(std::monostate()) or rhs == valeur_ou_infini<T>(std::monostate())) {
            return valeur_ou_infini<T>(std::monostate());
        } else {
            return std::get<T>(lhs.val) + std::get<T>(rhs.val);
        }
    };

    template <typename U>
    friend double operator/(const valeur_ou_infini<U> &lhs, const double &rhs)  {
        if (lhs == valeur_ou_infini<T>(std::monostate())) {
            if (rhs != HUGE_VAL) {
                return HUGE_VAL;
            } else {
                return NAN;
            }
        } else {
            return static_cast<double>(lhs.val) / rhs;
        }
    };
    //valeur_ou_infini<T>& operator=(T rhs);

    std::variant<T,std::monostate> val;

};

template <typename T>
static const valeur_ou_infini<T> INFINI = valeur_ou_infini<T>(std::monostate());

#endif //RUSHHOUR_MICHEL_JUSSEY_CARLE_INT_OR_INFINITY_H

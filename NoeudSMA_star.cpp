//
// Created by symeon on 22/01/19.
//

#include "NoeudSMA_star.h"

void NoeudSMA_star::genererSuccesseurs() {
    successeurs.clear();
    for (const auto& mouvement : p.getMouvements()) {
        if (parent.has_value()) {
            if ((*parent)->p != mouvement) {
                successeurs.insert(mouvement);
            }
        } else {
            successeurs.insert(mouvement);
        }
    }
}

std::optional<NoeudSMA_star> NoeudSMA_star::prochainSuccesseur() {
    if (not successeursGeneres) {
        successeursGeneres = true;
        this->genererSuccesseurs();
    }
    if (dernier_successeur_genere) {
        auto plateau_suivant = successeurs.upper_bound(*dernier_successeur_genere);
        if (plateau_suivant != successeurs.end()) {
            dernier_successeur_genere = *plateau_suivant;
            return NoeudSMA_star(std::make_shared<NoeudSMA_star>(*this),*plateau_suivant,this->g+1);
        } else {
            return {};
        }
    } else {
        if (not successeurs.empty()) {
            auto plateau_suivant = successeurs.begin();
            dernier_successeur_genere = *plateau_suivant;
            return NoeudSMA_star(std::make_shared<NoeudSMA_star>(*this),*plateau_suivant,this->g+1);
        } else {
            return {};
        }
    }
}

NoeudSMA_star::NoeudSMA_star(std::optional<std::shared_ptr<NoeudSMA_star>> parent,Plateau p, int g):
    parent(parent),
    p(p),
    g(g)
{
    f = g + this->p.calculerH();
}

bool NoeudSMA_star::operator<(const NoeudSMA_star &rhs) const {
    if (this->f != rhs.f) {
        return this->f < rhs.f;
    }
    if (this->g != rhs.g) {
        return this->g < rhs.g;
    }
    return this->p != rhs.p;
}

void NoeudSMA_star::backup() {
    if (aGenereTousSesSuccesseurs() && parent.has_value()) {
        std::list<Plateau> voisins = p.getMouvements();
        std::list<Plateau> successeurs;
        for (auto const& pl : voisins) {
            if (pl != (*parent)->p) {
                successeurs.push_back(pl);
            }
        }
        int min_cout_f = std::numeric_limits<int>::max();
        for (auto const& pl : successeurs) {
            int cout_f = this->g + 1 + pl.calculerH();
            if (cout_f < min_cout_f) {
                min_cout_f = cout_f;
            }
        }
        if (min_cout_f != this->f) {
            this->f = min_cout_f;
            (*parent)->backup();
        } else {
            this->f = min_cout_f;
        }
    }
}

bool NoeudSMA_star::aGenereTousSesSuccesseurs() {
    if (not successeursGeneres) {
        this->genererSuccesseurs();
    }
    if (dernier_successeur_genere) {
        return successeurs.upper_bound(*dernier_successeur_genere) == successeurs.end();
    } else {
        return successeurs.empty();
    }
}

//
// Created by Symeon on 11/01/2019.
//

#ifndef RUSHHOUR_MICHEL_JUSSEY_CARLE_PLATEAU_H
#define RUSHHOUR_MICHEL_JUSSEY_CARLE_PLATEAU_H


#include <list>
#include <ostream>
#include <array>
#include <map>
#include <set>
#include <sstream>
#include <fstream>
#include <utility>

enum enum_direction {
    VERTICAL,
    HORIZONTAL
};

class Voiture {

public:
    class coord {
    public:
        coord(int x, int y): x(x), y(y) {};
        explicit coord(Voiture v): x(v.getX()), y(v.getY()) {};
        int x;
        int y;

        bool isValid() {return x >= 0 and x < 6 and y >=0 and y < 6;};
        friend int operator>>(const coord &lhs, const coord &rhs);
        friend int operator<<(const coord &lhs, const coord &rhs) { return lhs >> rhs; };
        friend bool operator<(const coord &lhs, const coord &rhs);

        bool operator==(const coord &rhs) const;

        bool operator!=(const coord &rhs) const;
    };

    class voiture_coords_iterator: public std::iterator<
            std::input_iterator_tag,
            coord,
            int,
            coord*,
            coord
            >{
        const Voiture& v;
        int pos = 0;
    public:
        voiture_coords_iterator(const Voiture& v, int pos) : v(v), pos(pos) {};
        voiture_coords_iterator& operator++() {++pos; return *this;};
        bool operator==(voiture_coords_iterator other) const {return v == other.v and pos == other.pos;};
        bool operator!=(voiture_coords_iterator other) const {return !(*this == other);};
        reference operator*() const {
            switch (v.getDirection()) {
                case HORIZONTAL:
                    return coord(v.getX()+pos,v.getY());
                case VERTICAL:
                    return coord(v.getX(),v.getY()+pos);
            }
        };
    };

    explicit Voiture(char name='x', int x=0, int y=0, int longeur=1, enum_direction direction=HORIZONTAL);

    char getName() const { return name; };
    int getX() const;
    int getY() const;
    int getLongeur() const;
    enum_direction getDirection() const;
    voiture_coords_iterator begin() const;
    voiture_coords_iterator end() const;

    bool operator==(const Voiture &rhs) const;
    bool operator!=(const Voiture &rhs) const;

private:
    char name; 
    int x;
    int y;
    int longeur;
    enum_direction direction;

};

struct ordreDesCoordonees {
    bool operator()(const Voiture& v1, const Voiture& v2) const;
};

class Plateau {
public:
    explicit Plateau(std::set<Voiture,ordreDesCoordonees> new_voitures = {},Voiture voiture_joueur = Voiture());
    explicit Plateau(std::array<std::array<char,6>,6> array);
    explicit Plateau(std::istream& fichier);

    void ajouterVoiture(Voiture v);
    std::array<std::array<char,6>,6> toCharArray() const;
    std::string toString() const;

    friend std::istream &operator>>(std::istream &is, Plateau& p);
    friend std::ostream &operator<<(std::ostream &os, const Plateau &plateau1);

    bool operator<(const Plateau& rhs) const;
    bool operator==(const Plateau& rhs) const;
    bool operator!=(const Plateau& rhs) const;

    std::set<Voiture,ordreDesCoordonees>::const_iterator begin() const {return voitures_non_joueur.cbegin();};
    std::set<Voiture,ordreDesCoordonees>::const_iterator end() const {return voitures_non_joueur.cend();};

    std::list<Plateau> getMouvements();

    int calculerH() const;

    void init(std::istream &fichier);
    void init(std::array<std::array<char, 6>, 6> array);

    std::set<Voiture,ordreDesCoordonees> voitures_non_joueur;
    Voiture voiture_joueur;
};

class iterateur_mouvements_plateau: public std::iterator<std::input_iterator_tag,Plateau> {
    Plateau p;
    std::set<Voiture,ordreDesCoordonees>::const_iterator iterator_voiture;
    std::optional<int> mouvement;
public:
    iterateur_mouvements_plateau(Plateau p): p(std::move(p)), iterator_voiture(this->p.begin()) {operator++();};
    iterateur_mouvements_plateau(const iterateur_mouvements_plateau& imp);
    iterateur_mouvements_plateau& operator++();
};

namespace std {

    template <>
    class hash<Plateau>{
    public :
        size_t operator()(const Plateau &p ) const{
            size_t res = 1;
            for (auto v : p) {
                res *= 51;
                res += std::hash<char>()(v.getName());
                res *= 51;
                res += std::hash<int>()(v.getX());
                res *= 51;
                res += std::hash<int>()(v.getY());
                res *= 51;
                res += std::hash<int>()(v.getLongeur());
                res *= 51;
                res += std::hash<int>()(v.getDirection());
            }
            res *= 51;
            res += std::hash<char>()(p.voiture_joueur.getName());
            res *= 51;
            res += std::hash<int>()(p.voiture_joueur.getX());
            res *= 51;
            res += std::hash<int>()(p.voiture_joueur.getY());
            res *= 51;
            res += std::hash<int>()(p.voiture_joueur.getLongeur());
            res *= 51;
            res += std::hash<int>()(p.voiture_joueur.getDirection());
            return res;
        }
    };
}

#endif //RUSHHOUR_MICHEL_JUSSEY_CARLE_PLATEAU_H

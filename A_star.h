//
// Created by symeon on 22/01/19.
//

#ifndef RUSHHOUR_MICHEL_JUSSEY_CARLE_A_STAR_H
#define RUSHHOUR_MICHEL_JUSSEY_CARLE_A_STAR_H

#include <unordered_set>
#include <unordered_map>
#include "Plateau.h"

struct A_star {
    static std::list<Plateau> remonterChemin(Plateau fin, std::unordered_map<Plateau,Plateau> precedent);
    static std::optional<std::list<Plateau>> chercherSolution(Plateau p);
};

template <typename K, typename V>
V GetWithDef(const  std::map <K,V> & m, const K & key, const V & defval );


#endif //RUSHHOUR_MICHEL_JUSSEY_CARLE_A_STAR_H

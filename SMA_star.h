//
// Created by symeon on 22/01/19.
//

#ifndef RUSHHOUR_MICHEL_JUSSEY_CARLE_SMA_STAR_H
#define RUSHHOUR_MICHEL_JUSSEY_CARLE_SMA_STAR_H


#include "Plateau.h"
#include "NoeudSMA_star.h"

struct SMA_star {
    static std::optional<NoeudSMA_star> chercherSolution(Plateau p,int max_memoire, int profondeur_max);
};


#endif //RUSHHOUR_MICHEL_JUSSEY_CARLE_SMA_STAR_H

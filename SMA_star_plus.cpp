//
// Created by symeon on 25/01/19.
//

#include <unordered_map>
#include <iostream>
#include "SMA_star_plus.h"
#include "Noeud_SMA_star_plus.h"
#include "Open_Set_SMA_star_plus.h"

std::optional<std::list<Plateau>> SMA_star_plus::chercherSolution(Plateau p, size_t limite_memoire) {

    Open_Set_SMA_star_plus open;
    std::unordered_set<Noeud_SMA_star_plus> allNodes;
    std::unordered_map<Plateau,Plateau> precedent;

    open.insert(Noeud_SMA_star_plus(p));
    size_t u = 1;

    while (not open.empty()) {
        Noeud_SMA_star_plus b = open.minfCostNode();
        open.remove(b);

        if (b.plateau.calculerH() == 0) {
            return remonterParents(b.plateau,p,precedent);
        } else if (b.cout_f == INFINI<int>) {
            return {};
        }


        std::list<Noeud_SMA_star_plus> N;

        if (b.aEteEtendu) {
            for (const auto& paire : b.couts_f_oublies) {
                precedent[paire.first] = b.plateau;
                N.emplace_back(paire.first,b.cout_g+1,b.plateau);
            }
        } else {
            b.etendre();
            for (auto const& plateau : b.successeurs) {
                precedent[plateau] = b.plateau;
                N.emplace_back(plateau,b.cout_g+1,b.plateau);
            }
        }

        for (auto& n : N) {
            if (b.couts_f_oublies.count(n.plateau) != 0) {
                n.cout_f = b.couts_f_oublies.at(n.plateau);
                b.couts_f_oublies.erase(n.plateau);
            } else if (n.plateau.calculerH() != 0 and (n.plateau.getMouvements().empty() or n.cout_g > limite_memoire -1)) {
                n.cout_f = INFINI<int>;
            } else {
                n.cout_f = b.cout_f>(n.cout_g+n.plateau.calculerH())?b.cout_f:n.cout_g+n.plateau.calculerH();
            }
            std::cout << '\r' << "Profondeur en cours : " << n.cout_g;
            open.insert(n);
            u++;
        }


        while (u > limite_memoire) {

            std::optional<Noeud_SMA_star_plus> w = open.maxcCostLeaf();
            if (not w.has_value()) {
                w = open.maxcCostNode();
            }
            open.remove(*w);

            if (w->parent) {
                std::optional<Noeud_SMA_star_plus> parent = open.find(*w->parent);
                if (parent) {
                    open.remove(*parent);
                    parent->successeurs.erase(w->plateau);
                    parent->couts_f_oublies.insert_or_assign(w->plateau,w->cout_f);
                    parent->cout_f = parent->min_des_couts_f_oublies();
                    open.insert(*parent);
                }
            }

            --u;
        }
    }

    if (open.empty()) {
        return {};
    }

}

std::list<Plateau> SMA_star_plus::remonterParents(Plateau fin, Plateau debut, std::unordered_map<Plateau,Plateau> precedent) {
    std::list<Plateau> liste = {fin};
    while (fin != debut and precedent.count(fin) != 0) {
        fin = precedent[fin];
        liste.push_front(fin);
    }
    return liste;
}

/*
std::list<Noeud_SMA_star_plus> SMA_star_plus::remonterParents(Noeud_SMA_star_plus n) {
    std::list<Noeud_SMA_star_plus> liste = {n};
    auto parent = n.parent;
    while (parent) {
        liste.push_front(parent->get());
        parent = parent->get().parent;
    }
    return liste;
}
*/
//
// Created by symeon on 22/01/19.
//

#include "A_star.h"
#include "valeur_ou_infini.h"

std::list<Plateau> A_star::remonterChemin(Plateau fin, std::unordered_map<Plateau,Plateau> precedent) {
    std::list<Plateau> liste {fin};
    Plateau &p = fin;
    while (precedent.count(p) != 0) {
        p = precedent[p];
        liste.push_front(p);
    }
    return liste;
}

std::optional<std::list<Plateau>> A_star::chercherSolution(Plateau p) {

    std::unordered_map<Plateau,valeur_ou_infini<int>> g;
    std::unordered_map<Plateau,valeur_ou_infini<int>> f;
    std::unordered_set<Plateau> closedSet;
    std::unordered_set<Plateau> openSet;
    std::unordered_map<Plateau,Plateau> precedent;

    g.emplace(p,0);
    f.emplace(p,p.calculerH());
    openSet.insert(p);

    while (!openSet.empty()) {
        auto meilleur = *(openSet.begin());
        for (auto const & _ : openSet) {
            if (f.at(_) < f.at(meilleur)) {
                meilleur = _;
            }
        }
        if (meilleur.calculerH() == 0) {
            return remonterChemin(meilleur,precedent);
        }
        openSet.erase(meilleur);
        closedSet.insert(meilleur);

        for (auto voisin : meilleur.getMouvements()) {

            if (precedent.count(meilleur) != 0) {
                if (precedent[meilleur] == voisin) {
                    continue;
                }
            }

            if (closedSet.count(voisin) != 0) {
                continue;
            }

            valeur_ou_infini<int> g_previsionel = g.at(meilleur) + 1;

            if (openSet.count(voisin) == 0) {
                openSet.insert(voisin);
            } else if (g_previsionel >= g.at(voisin)) {
                continue;
            }

            precedent.emplace(voisin,meilleur);
            if (g.count(voisin) != 0) {
                g.at(voisin) = g_previsionel;
            } else {
                g.insert({voisin,g_previsionel});
            }

            if (f.count(voisin) != 0) {
                f.at(voisin) = g_previsionel + voisin.calculerH();
            } else {
                f.insert({voisin,g_previsionel + voisin.calculerH()});
            }
        }
    }
    return {};
}

// merci stack overflow !
template <typename K, typename V>
V GetWithDef(const  std::map <K,V> & m, const K & key, const V & defval ) {
    typename std::map<K,V>::const_iterator it = m.find( key );
    if ( it == m.end() ) {
        return defval;
    }
    else {
        return it->second;
    }
}

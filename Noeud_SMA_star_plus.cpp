#include <utility>

#include <utility>

//
// Created by symeon on 24/01/19.
//

#include "Noeud_SMA_star_plus.h"

Noeud_SMA_star_plus::Noeud_SMA_star_plus(const Plateau &p, const int &g, std::optional<Plateau> parent) :
    plateau(p),
    cout_g(g),
    cout_f(g + p.calculerH()),
    parent(std::move(parent))
    {}

void Noeud_SMA_star_plus::etendre() {
    successeurs.clear();
    for (auto const& p : plateau.getMouvements()) {
        successeurs.insert(p);
    }
    aEteEtendu = true;
}

double Noeud_SMA_star_plus::cout_c() const {
    if (cout_f == INFINI<int>) {
        return HUGE_VAL;
    } else {
        return static_cast<double>(std::get<int>(cout_f.val)) / log(static_cast<double>(cout_g) + exp(1.0));
    }
}

valeur_ou_infini<int> Noeud_SMA_star_plus::min_des_couts_f_oublies() {
    valeur_ou_infini<int> resultat = INFINI<int>;
    for (auto paire : couts_f_oublies) {
        if (paire.second < resultat) {
            resultat = paire.second;
        }
    }
    return resultat;
}

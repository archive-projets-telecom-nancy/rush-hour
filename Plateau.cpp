//
// Created by Symeon on 11/01/2019.
//

#include <functional>
#include "Plateau.h"

bool operator<(const Voiture::coord &lhs, const Voiture::coord &rhs) {
    if (lhs.y < rhs.y)
        return true;
    if (rhs.y < lhs.y)
        return false;
    return lhs.x < rhs.x;
}

/*
 * Donne la distance en norme 1
 */
int operator>>(const Voiture::coord &lhs, const Voiture::coord &rhs) {
    return abs(lhs.x - rhs.x) + abs(lhs.y - rhs.y);
}

bool Voiture::coord::operator==(const coord &rhs) const {
    return x == rhs.x &&
           y == rhs.y;
}

bool Voiture::coord::operator!=(const coord &rhs) const {
    return !(rhs == *this);
}

/*
 * Vérfie qu'aucune voiture dans la liste ne se chevauche
 */
Plateau::Plateau(std::set<Voiture,ordreDesCoordonees> new_voitures,Voiture voiture_joueur): voiture_joueur(voiture_joueur) {
    std::array<std::array<bool,6>,6> cases_occupees = {};

    // C'est la première voiture sur le plateau, on suppose qu'il n'y a pas de problème ...
    for (auto c : voiture_joueur) {
        cases_occupees[c.y][c.x] = true;
    }

    for (auto voiture : new_voitures) {
        for (auto c : voiture) {
            if (cases_occupees[c.y][c.x]) {
                throw std::invalid_argument(
                        "Les voitures passées en argument à la construction du plateau se chevauchent");
            } else {
                cases_occupees[c.y][c.x] = true;
            }
        }
    }

    for (auto voiture : new_voitures) {
        voitures_non_joueur.insert(voiture);
    }
}

/*
 * Crée un plateau depuis un tableau de caractères, typiquement lus depuis un fichier
 */
Plateau::Plateau(std::array<std::array<char, 6>, 6> array) {
    init(array);
}

void Plateau::ajouterVoiture(Voiture v) {
    auto cases = this->toCharArray();
    for (auto c : v) {
        if (cases[c.y][c.x] != '.') {
            throw std::invalid_argument("Impossible d'ajouter la voiture sans collision");
        }
    }
    this->voitures_non_joueur.insert(v);
}

std::array<std::array<char, 6>, 6> Plateau::toCharArray() const {

    std::array<std::array<char, 6>, 6> cases = {};
    for (auto& ligne : cases) {
        std::fill(ligne.begin(),ligne.end(),'.');
    }
    // std::string lettres = "abcdefghijklmnopqrstuvwyz";

/*
    if (this->voitures_non_joueur.size() > lettres.size()) {
        throw std::runtime_error("Le plateau n'est pas représentable, il y a plus de voitures que de lettres affichables");
    }
*/

    for (auto c : voiture_joueur) {
        cases[c.y][c.x] = 'x';
    }

    //int indice_lettre = 0;
    for (auto voiture : voitures_non_joueur) {
        for (auto c : voiture) {
            cases[c.y][c.x] = voiture.getName();
        }
        //indice_lettre++;
    }

    return cases;
}

Plateau::Plateau(std::istream& fichier) {
    init(fichier);
}

void Plateau::init(std::istream& fichier) {

    std::array<std::array<char, 6>, 6> res = {};

    std::string ligne;
    int index_ligne = 0;

    while (index_ligne < 6 and std::getline(fichier,ligne)) {
        std::istringstream iss(ligne);
        for (int i = 0; i < 6; ++i) {
            char c;
            if (not (iss >> c)) {
                std::stringstream err;
                err << "Plateau invalide : la ligne " << (index_ligne+1) << " ne contient pas 6 caractères";
                throw std::invalid_argument(err.str());
            } else {
                res[index_ligne][i] = c;
            }
        }
        index_ligne++;
    }

    if (index_ligne < 6) {
        throw std::invalid_argument("Plateau invalide : le plateau ne contient pas assez de lignes");
    }

    init(res);
}

void Plateau::init(std::array<std::array<char, 6>, 6> array) {

    this->voitures_non_joueur.clear();

    std::map<char,std::set<Voiture::coord>> coordonees_par_lettres;

    for (int y = 0; y < 6; ++y) {
        for (int x = 0; x < 6; ++x) {
            if (array[y][x] != '.') {
                if (coordonees_par_lettres.count(array[y][x]) == 0) {
                    coordonees_par_lettres[array[y][x]] = {Voiture::coord(x,y)};
                } else {
                    coordonees_par_lettres[array[y][x]].insert(Voiture::coord(x,y));
                }
            }
        }
    }

    // Si il n'y a pas la voiture du joueur on plante
    if (coordonees_par_lettres.count('x') != 1) {
        throw std::invalid_argument("Le plateau ne contient pas la voiture du joueur");
    }

    // Si on s'en réfère à l'ordre sur les case si deux cases d'une même voiture
    // qui se suivent on une distance de plus de 1 alors le plateau est invalide
    for (const auto& paire : coordonees_par_lettres) {
        if (paire.second.size() >= 2) {
            for (
                    auto premier = paire.second.begin(),
                    suivant = ++paire.second.begin();
                    suivant != paire.second.end();
                    ++premier,
                    ++suivant
                    ) {
                if ((*premier >> *suivant) > 1) {
                    std::stringstream err;
                    err << "Plateau invalide : la lettre " << paire.first << " est présente sur deux cases disjointes";
                    throw std::invalid_argument(err.str());
                }
            }
        }
    }

    // sinon ça a l'air bon, on a qu'a lire et ajouter
    for (const auto& paire : coordonees_par_lettres) {

        enum_direction direction = HORIZONTAL;
        auto premier = paire.second.begin();

        if (paire.second.size() >= 2) {
            auto suivant = ++paire.second.begin();
            if (premier->x == suivant->x) {
                direction = VERTICAL;
            } else {
                direction = HORIZONTAL;
            }
        }
        if (paire.first != 'x') {
            ajouterVoiture(Voiture(paire.first, premier->x,premier->y,paire.second.size(),direction));
        } else {
            voiture_joueur = Voiture('x', premier->x,premier->y,paire.second.size(),direction);
        }
    }
}

bool Plateau::operator==(const Plateau &rhs) const {
    return voitures_non_joueur == rhs.voitures_non_joueur &&
           voiture_joueur == rhs.voiture_joueur;
}

bool Plateau::operator!=(const Plateau &rhs) const {
    return !(rhs == *this);
}

int Voiture::getLongeur() const {
    return longeur;
}

enum_direction Voiture::getDirection() const {
    return direction;
}

int Voiture::getX() const {
    return x;
}

int Voiture::getY() const {
    return y;
}

Voiture::Voiture(char name, int x, int y, int longeur, enum_direction direction): name(name),x(x),y(y),longeur(longeur), direction(direction) {
    if (x < 0 or x > 5) {
        throw std::invalid_argument("x invalide pendant la création de la voiture");
    }
    if (y < 0 or y > 5) {
        throw std::invalid_argument("y invalide pendant la création de la voiture");
    }
    switch (direction) {
        case HORIZONTAL:
            if (x+longeur-1 > 5) {
                throw std::invalid_argument("voiture trop longue");
            }
            break;
        case VERTICAL:
            if (y+longeur-1 > 5) {
                throw std::invalid_argument("voiture trop longue");
            }
    }
}

bool Voiture::operator==(const Voiture &rhs) const {
    return x == rhs.x &&
           y == rhs.y &&
           longeur == rhs.longeur &&
           direction == rhs.direction;
}

bool Voiture::operator!=(const Voiture &rhs) const {
    return !(rhs == *this);
}

Voiture::voiture_coords_iterator Voiture::begin() const {
    return voiture_coords_iterator(*this, 0);
}

Voiture::voiture_coords_iterator Voiture::end() const {
    return Voiture::voiture_coords_iterator(*this, this->getLongeur());
}

bool ordreDesCoordonees::operator()(const Voiture &v1, const Voiture &v2) const {
    if (v1.getY() != v2.getY()) {
        return v1.getY() < v2.getY();
    } else if (v1.getX() != v2.getX()) {
        return v1.getX() < v2.getX();
    } else {
        return false;
    }
}

iterateur_mouvements_plateau::iterateur_mouvements_plateau(const iterateur_mouvements_plateau &imp): p(imp.p), mouvement(imp.mouvement) {

    for (auto voiture_imp = imp.p.begin(), voiture_interne = this->p.begin();
        voiture_imp != imp.p.end() and voiture_interne != this->p.end();
        ++voiture_imp, ++voiture_interne
        ) {
        if (voiture_imp == imp.iterator_voiture) {
            this->iterator_voiture = voiture_interne;
            break;
        }
    }
}

std::list<Plateau> Plateau::getMouvements() {

    std::list<Plateau> liste;

    /*
     * head renvoie la coordonée de tête de la voiture
     * verif renvoie la coordonée a verifier pour les collisions et la validité par rapport aux bords
     * move crée un nouveau plateau ou la voiture old_v est remplacéepar  la nouvelle new_v
     *      (différent si c'est la voiture du joueur ou pas)
     */
    auto ajouter_pour_mouvement =
            [this]
            (
                    std::function<Voiture::coord(Voiture,int)> head,
                    std::function<Voiture::coord(Voiture,int)> verif,
                    std::function<void(Voiture old_v,Voiture new_v,Plateau& p)> move,
                    Voiture v
            )
            -> std::list<Plateau>
    {
        std::list<Plateau> sous_liste;
        for (int i = 1; verif(v,i).isValid(); ++i) {
            bool espacePris = false;
            for (auto autre_voiture : voitures_non_joueur) {
                if (autre_voiture != v) {
                    for (auto coordonee : autre_voiture) {
                        if (coordonee == verif(v,i)) {
                            espacePris = true;
                            break;
                        }
                    }
                }
                if (espacePris) {
                    break;
                }
            }
            // ne pas oublier de verifier la voiture joueur également !
            for (auto coordonee : this->voiture_joueur) {
                if (coordonee == verif(v,i)) {
                    espacePris = true;
                    break;
                }
            }
            if (espacePris) {
                break;
            } else {
                Voiture::coord new_head = head(v,i);
                Voiture nouvelle_voiture(v.getName(),new_head.x,new_head.y,v.getLongeur(),v.getDirection());
                Plateau nouveau_plateau(*this);
                move(v,nouvelle_voiture,nouveau_plateau);
                sous_liste.push_back(nouveau_plateau);
            }
        }
        return sous_liste;
    };

    std::function<Voiture::coord(Voiture,int)> head_gauche  = [](Voiture v, int i) -> Voiture::coord { return Voiture::coord(v.getX()-i,v.getY());};

    std::function<Voiture::coord(Voiture,int)> head_droite  = [](Voiture v, int i) -> Voiture::coord { return Voiture::coord(v.getX()+i,v.getY());};
    std::function<Voiture::coord(Voiture,int)> verif_droite = [](Voiture v, int i) -> Voiture::coord { return Voiture::coord(v.getX()+v.getLongeur()-1+i,v.getY());};

    std::function<Voiture::coord(Voiture,int)> head_haut    = [](Voiture v, int i) -> Voiture::coord { return Voiture::coord(v.getX(),v.getY()-i);};

    std::function<Voiture::coord(Voiture,int)> head_bas     = [](Voiture v, int i) -> Voiture::coord { return Voiture::coord(v.getX(),v.getY()+i);};
    std::function<Voiture::coord(Voiture,int)> verif_bas    = [](Voiture v, int i) -> Voiture::coord { return Voiture::coord(v.getX(),v.getY()+v.getLongeur()-1+i);};

    // Pour toutes les voitures sauf le joueur
    std::function<void(Voiture,Voiture,Plateau&)> move_nj =
            []
            (Voiture old_v,Voiture new_v,Plateau& p)
            {
                p.voitures_non_joueur.erase(old_v);
                p.ajouterVoiture(new_v);
            };

    // Pour la voiture du joueur
    std::function<void(Voiture,Voiture,Plateau&)> move_j =
            []
                    (Voiture old_v,Voiture new_v,Plateau& p)
            {
                p.voiture_joueur = new_v;
            };

    for (auto const& voit : voitures_non_joueur) {
        switch (voit.getDirection()) {
            case HORIZONTAL:
                // On essaye de bouger à gauche
                liste.splice(liste.cend(),ajouter_pour_mouvement(head_gauche,head_gauche,move_nj,voit));
                // On essaye de bouger à droite
                liste.splice(liste.cend(),ajouter_pour_mouvement(head_droite,verif_droite,move_nj,voit));
                break;
            case VERTICAL:
                // On essaye de bouger vers le haut
                liste.splice(liste.cend(),ajouter_pour_mouvement(head_haut,head_haut,move_nj,voit));
                // On essaye de bouger vers le bas
                liste.splice(liste.cend(),ajouter_pour_mouvement(head_bas,verif_bas,move_nj,voit));
                break;
        }
    }
    switch (voiture_joueur.getDirection()) {
        case HORIZONTAL:
            // On essaye de bouger à gauche
            liste.splice(liste.cend(),ajouter_pour_mouvement(head_gauche,head_gauche,move_j,voiture_joueur));
            // On essaye de bouger à droite
            liste.splice(liste.cend(),ajouter_pour_mouvement(head_droite,verif_droite,move_j,voiture_joueur));
            break;
        case VERTICAL:
            // On essaye de bouger vers le haut
            liste.splice(liste.cend(),ajouter_pour_mouvement(head_haut,head_haut,move_j,voiture_joueur));
            // On essaye de bouger vers le bas
            liste.splice(liste.cend(),ajouter_pour_mouvement(head_bas,verif_bas,move_j,voiture_joueur));
            break;
    }
    return liste;
}

std::ostream &operator<<(std::ostream &os, const Plateau &plateau1) {
    int line_index = 0;
    os << "╭──────╮" << std::endl;
    for (const auto& line : plateau1.toCharArray()) {
        os << "│";
        for (const auto& c : line) {
            os << c;
        }
        if (line_index != 2) {
            os << "│";
        }
        os << std::endl;
        line_index++;
    }
    os << "╰──────╯";
    return os;
}

std::istream &operator>>(std::istream &is, Plateau &p) {
    p.init(is);
}

int Plateau::calculerH() const {
    int nombre_obsctacles = 0;
    for (auto voiture : voitures_non_joueur) {
        for (auto c : voiture) {
            if (c.y == voiture_joueur.getY() and c.x >= voiture_joueur.getX()+voiture_joueur.getLongeur()) {
                nombre_obsctacles++;
            }
        }
    }
    return nombre_obsctacles;
}

/*
 * ça marche que dans notre cas
 */
bool Plateau::operator<(const Plateau &rhs) const {

    if (this->voitures_non_joueur.size() != rhs.voitures_non_joueur.size()) {
        return this->voitures_non_joueur.size() < rhs.voitures_non_joueur.size();
    } else {

        std::map<char,Voiture> map_lhs;
        std::map<char,Voiture> map_rhs;

        for (auto v : this->voitures_non_joueur) {
            map_lhs.insert({v.getName(),v});
        }
        for (auto v : rhs.voitures_non_joueur) {
            map_rhs.insert({v.getName(),v});
        }

        for (auto paire : map_lhs) {
            if (map_rhs.find(paire.first) != map_rhs.end()) {
                if (map_rhs[paire.first] != paire.second) {
                    return true;
                }
            } else {
                return true;
            }
        }
        return this->voiture_joueur != rhs.voiture_joueur;
    }
}

std::string Plateau::toString() const {
    std::stringstream ss;
    for (auto _ : toCharArray()) {
        for (auto c : _) {
            ss << c;
        }
        ss << '\n';
    }
    return ss.str();
}


iterateur_mouvements_plateau &iterateur_mouvements_plateau::operator++() {

    if (mouvement) {

    } else {
        for (auto const& voiture : p) {
            switch (voiture.getDirection()) {
                case HORIZONTAL:
                    break;
                case VERTICAL:
                    break;
            }
        }
    }
}

//
// Created by symeon on 22/01/19.
//

#include <iostream>
#include "SMA_star.h"

std::optional<NoeudSMA_star> SMA_star::chercherSolution(Plateau p, int max_memoire, int profondeur_max) {
    std::set<NoeudSMA_star> open;
    NoeudSMA_star depart({},p,0);
    open.insert(depart);

    int used = 1;

    while(!open.empty()) {
        NoeudSMA_star best(*open.begin());
        if (best.p.calculerH() == 0) {
            return best;
        }

        std::optional<NoeudSMA_star> s = best.prochainSuccesseur();
        if (s) {
            if (s->p.calculerH() != 0 && s->g > profondeur_max) {
                s->f = std::numeric_limits<int>::max();
            } else {
                if (best.f > s->f) {
                    s->f = best.f;
                }
            }
        } else {
            best.backup();

            // réinserer best dans la queue et tous ses parents
            NoeudSMA_star old_best(best);
            auto it = open.find(old_best);
            if (it != open.end()) {
                open.erase(it);
            } else {
                throw std::runtime_error("Impossible de retrouver l'ancien noeud dans la queue après backup");
            }
            while (old_best.parent.has_value()) {
                old_best = **(old_best.parent);
                auto it = open.find(old_best);
                if (it != open.end()) {
                    open.erase(it);
                } else {
                    throw std::runtime_error("Impossible de retrouver l'ancien noeud dans la queue après backup");
                }
            }
            NoeudSMA_star aInserer(best);
            open.insert(aInserer);
            while (aInserer.parent.has_value()) {
                aInserer = **(aInserer.parent);
                open.insert(aInserer);
            }

        }

        bool tousLesSuccesseursSontEnMemoire = true;
        for (auto const& plateau : best.successeurs) {
            bool trouve = false;
            for (auto const& noeud : open) {
                if (noeud.p == plateau) {
                    trouve = true;
                }
            }
            if (not trouve) {
                tousLesSuccesseursSontEnMemoire = false;
                break;
            }
        }

        if (tousLesSuccesseursSontEnMemoire) {
            auto it = open.find(best);
            if (it != open.end()) {
                open.erase(it);
            }
        }

        used++;
        std::cout << "\r" << used;

        if (used > max_memoire) {
            auto it = std::prev(open.end());
            auto parent = it->parent;
            if (parent) {
                (*(parent))->successeurs.erase(it->p);
                bool trouve = false;
                for (auto const& noeud : open) {
                    if (noeud.p == (*parent)->p) {
                        trouve = true;
                        break;
                    }
                }
                if (not trouve) {
                    open.insert(**parent);
                }
            }
            open.erase(it);
            used--;
        }

        if (s) {
            open.insert(*s);
        }
    }
    // FAIL
}
